package au.edu.unsw.soacourse.ass1;

import javax.jws.WebService;

@WebService
public interface HelloWorld {
    String sayHi(String text);
}

