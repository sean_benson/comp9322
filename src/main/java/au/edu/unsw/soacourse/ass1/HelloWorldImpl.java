
package au.edu.unsw.soacourse.ass1;

import javax.jws.WebService;

@WebService(endpointInterface = "au.edu.unsw.soacourse.ass1.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

