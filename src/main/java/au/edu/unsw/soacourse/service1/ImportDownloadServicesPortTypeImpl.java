package au.edu.unsw.soacourse.service1;

import au.edu.unsw.sltf.services.ImportDownloadFault;
import au.edu.unsw.sltf.services.ImportDownloadFaultType;
import au.edu.unsw.sltf.services.ImportDownloadFault_Exception;
import au.edu.unsw.sltf.services.ImportDownloadServicesPortType;

import org.apache.commons.csv.*;

import java.net.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import java.io.*;

public class ImportDownloadServicesPortTypeImpl implements
		ImportDownloadServicesPortType {

	@Override
	public String downloadFile(String eventSetId, String fileType)
			throws ImportDownloadFault_Exception {
		// TODO Auto-generated method stub
		//return the URI of the eventsetid if the file exists, otherwise produce a fault
		File f = new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+eventSetId+".csv");
		if(!f.exists() || f.isDirectory()) { 
			throw new ImportDownloadFault_Exception();
		}
		if(!(fileType.equals("csv")||fileType.equals("xml")||fileType.equals("html"))){
			ImportDownloadFault fault = new ImportDownloadFault();
			fault.setFaultType(ImportDownloadFaultType.INVALID_FILE_TYPE);
			fault.setFaultMessage("File type need to be csv, xml or html");
			throw new ImportDownloadFault_Exception("Error in file type", fault);
		}
		// TODO convert to html or xml here
		if(fileType.equals("xml")){
			CSVtoXML conv = new CSVtoXML(eventSetId+".csv");
			try {
				conv.write();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				ImportDownloadFault fault = new ImportDownloadFault();
				fault.setFaultType(ImportDownloadFaultType.PROGRAM_ERROR);
				fault.setFaultMessage("Server IO error");
				throw new ImportDownloadFault_Exception("Server IO error", fault);
				
			}
		}
		if(fileType.equals("html")){
			CSVtoHTML conv = new CSVtoHTML(eventSetId+".csv");
			try {
				conv.write();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				ImportDownloadFault fault = new ImportDownloadFault();
				fault.setFaultType(ImportDownloadFaultType.PROGRAM_ERROR);
				fault.setFaultMessage("Server IO error");
				throw new ImportDownloadFault_Exception("Server IO error", fault);
				
			}
		}
			
		//String url = 
		return "http://localhost:8080/"+eventSetId+"."+fileType;
	}

	@Override
	public String importMarketData(String sec, String startDate,
			String endDate, String dataSourceURL)
			throws ImportDownloadFault_Exception {
		//check sec code in the right format
		if(!Pattern.matches("^[A-Z]{3}[A-Z]?$",sec)){
			ImportDownloadFault f = new ImportDownloadFault();
			f.setFaultType(ImportDownloadFaultType.INVALID_SEC_CODE);
			f.setFaultMessage("SEC code not in required XXX or XXXX format");
			throw new ImportDownloadFault_Exception("Error in sec code", f);
		};
		
		//fetch URL
		String event_id = setID(sec, startDate, endDate, dataSourceURL);
		try {
			URL csv = new URL(dataSourceURL);
			BufferedReader in = new BufferedReader(
		        new InputStreamReader(csv.openStream()));
			filterMarketData(sec, startDate, endDate, in, event_id);
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ImportDownloadFault f = new ImportDownloadFault();
			f.setFaultType(ImportDownloadFaultType.INVALID_URL);
			f.setFaultMessage("the input URL is either in the wrong format, or not reachable");
			throw new ImportDownloadFault_Exception("Error trying to reach the URL provided", f);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ImportDownloadFault_Exception("IO exception opening csv stream", e);
		}
		
		//parse csv file according to filter
		//saving each line to the eventid file if it matches
		
		
		
		// TODO Auto-generated method stub
		//implement simple filter
		//save file to disk
		//return eventid
		return event_id;
	}
	
	private String setID(String sec, String startDate,
			String endDate, String dataSourceURL){
		//make a hash from these and the current time
		//need to include current time to stop updates overiding existing files
		Date date = new Date(); 
		String current_time = date.toString();
		
		String startDateBuf[] = startDate.split("-");
		String endDateBuf[] = endDate.split("-");

		//MMDDYYMMDDYYHASHHASH
		String eventId = startDateBuf[0]+startDateBuf[1]+
				startDateBuf[2].substring(Math.max(startDateBuf[2].length() - 2, 0));
		eventId = eventId+endDateBuf[0]+endDateBuf[1]+
				endDateBuf[2].substring(Math.max(endDateBuf[2].length() - 2, 0));;
		String comb = sec+dataSourceURL+current_time;
		eventId = eventId.concat(String.valueOf(comb.hashCode()));
		
		return eventId;
	}
	
	public void filterMarketData(String sec, String startDate,
			String endDate, BufferedReader in, String event_id) throws IOException, ImportDownloadFault_Exception{
		//work out the file path from the event ID
		BufferedWriter out = null;
		try  
		{
		    FileWriter fstream = new FileWriter(new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+event_id+".csv"), false); //false tells not to append data.
		    out = new BufferedWriter(fstream);
		    String inputLine;
		    inputLine = in.readLine();
		    boolean secFound = false;
	        while ((inputLine = in.readLine()) != null){
	            String data[] = inputLine.split(",",-1);
	            //if within date range and code add to file
	            SimpleDateFormat format = new SimpleDateFormat("DD-MM-YYYY");
	            Date start = format.parse(startDate);
	            Date end = format.parse(endDate);
	            if(end.before(start)){
	            	ImportDownloadFault f = new ImportDownloadFault();
	            	f.setFaultMessage("Start date after end date");
	            	f.setFaultType(ImportDownloadFaultType.INVALID_DATES);
	            	throw new ImportDownloadFault_Exception("Invalid Dates", f);
	            }
	            SimpleDateFormat csvDateFormat = new SimpleDateFormat("DD-MMM-YYYY");
	            Date csvDate = csvDateFormat.parse(data[1]);
	            //put in a logger
	            //need to flag if the sec code is not found in the file
	            if(data[0].equals(sec)) secFound = true;
	            
	            if(((csvDate.after(start)&&csvDate.before(end))||csvDate.equals(start)||csvDate.equals(end))&&
	            		data[0].equals(sec)){
	            out.write(inputLine+"\n");
	            }
	        }
	        in.close();
	        if(!secFound){
	        	ImportDownloadFault f = new ImportDownloadFault();
				f.setFaultType(ImportDownloadFaultType.INVALID_SEC_CODE);
				f.setFaultMessage("SEC code not found at url");
				throw new ImportDownloadFault_Exception("Error in sec code", f);
	        }
		    }
		catch (IOException | ParseException e)
		{
		    System.err.println("Error: " + e.getMessage());
		}
		finally
		{
		    if(out != null) {
		        out.close();
		    }
		}
		
		
		
		
		return;
	}
	


}
