package au.edu.unsw.soacourse.service1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CSVtoHTML {

	
		String filepath;
		public CSVtoHTML(String filepath){
			this.filepath = filepath;
		}
		
		public void write() throws IOException{
		//parse in CSV
			
			String inputLine = "";
			String event_id = filepath.split("\\.")[0];
			String output = header(event_id);
			File csv = new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+filepath);  
			FileReader input = new FileReader(csv);
			BufferedReader in = new BufferedReader(input);
			while ((inputLine = in.readLine()) != null){
				
				String data[] = inputLine.split(",",-1);
				output += dataToHTMLElement(data);
				
			}
	in.close();
			output+="</table>\n";
		//write
			FileWriter fstream = new FileWriter(new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+event_id+".html"), false); //false tells not to append data.
		    BufferedWriter out = new BufferedWriter(fstream);
		    out.write(output);
		    out.close();
		
		}
		
		private String dataToHTMLElement(String[] data){
			String line = "<tr><td>"+data[0]+"</td><td>"+ data[1]+ "</td><td>"+data[2]+
					"</td><td>"+data[3]+"</td><td>"+data[4]+"</td><td>"+data[5]+
					"</td><td>"+data[6]+"</td><td>"+data[7]+"</td><td>"+data[8]+
					"</td><td>"+data[9]+"</td><td>"+data[10]+"</td></tr>\n";
						
			return line;
			
		}
		
		private String header(String eventID){
			String head = "<!DOCTYPE html>\n<title>"+eventID+"</title>\n";
			head += "<table style=\"width:100%\">\n";
			//add column headings
			
			
			
			return head;
		}
		
		}


