package au.edu.unsw.soacourse.service1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;



//methods to convert marketdata csv to xml
//simple output format descibed as each transaction being an element with 9 attributes as per the csv
public class CSVtoXML {
	String filepath;
	public CSVtoXML(String filepath){
		this.filepath = filepath;
	}
	
	public void write() throws IOException{
	//parse in CSV
		
		String inputLine = "";
		String event_id = filepath.split("\\.")[0];
		String output = header(event_id);
		File csv = new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+filepath);  
		FileReader input = new FileReader(csv);
		BufferedReader in = new BufferedReader(input);
		while ((inputLine = in.readLine()) != null){
			
			String data[] = inputLine.split(",",-1);
			output += dataToXMLElement(data);
			
		}
in.close();
		output+="</importData>\n";
	//write
		FileWriter fstream = new FileWriter(new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+event_id+".xml"), false); //false tells not to append data.
	    BufferedWriter out = new BufferedWriter(fstream);
	    out.write(output);
	    out.close();
	
	}
	
	private String dataToXMLElement(String[] data){
		String line = "<data sec=\""+data[0]+"\" date=\""+ data[1]+ "\" time=\""+data[2]+
				"\" gmtoffset=\""+data[3]+"\" type=\""+data[4]+"\" price=\""+data[5]+
				"\" volume=\""+data[6]+"\" bidprice=\""+data[7]+"\" bidsize=\""+data[8]+
				"\" askprice=\""+data[9]+"\" asksize=\""+data[10]+"\">\n";
					
		return line;
		
	}
	
	private String header(String eventID){
		String head = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		head += "<importData eventid=\""+eventID+">\n";
		
		
		
		return head;
	}
	
	}

