package au.edu.unsw.soacourse.service3;

import javax.jws.WebService;

import au.edu.unsw.soacourse.service2.ExchangeRateFaultMsg;

@WebService
public interface MarketDataUtilService {

	public ConvertMarketDataResponseMsg ConvertMarketData(ConvertMarketDataRequestMsg request) throws ExchangeRateFaultMsg;
	
	public SummaryMarketDataResponse SummaryMarketData(SummaryMarketDataRequest request) throws ExchangeRateFaultMsg;
	
}
