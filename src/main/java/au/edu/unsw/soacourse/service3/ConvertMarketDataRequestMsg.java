package au.edu.unsw.soacourse.service3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConvertMarketDataRequestMsg", propOrder = {"eventSetId", "targetCurrency"})
public class ConvertMarketDataRequestMsg {

    @XmlElement(required = true)
    protected String eventSetId;
    
    @XmlElement(required = true)
    protected String targetCurrency;

    public String getEventSetId() {
        return eventSetId;
    }

    public void setEventSetId(String value) {
        this.eventSetId = value;
    }

    public String getTargetCurrency() {
    	return targetCurrency;
    }
    
    public void setTargetCurrency(String value) {
    	this.targetCurrency = value;
    }
}
