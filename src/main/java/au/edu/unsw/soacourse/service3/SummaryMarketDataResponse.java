package au.edu.unsw.soacourse.service3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "summaryMarketDataResponse", propOrder = {"eventSetId", "sec", "startDate", "endDate", "currencyCode", "numberOfLines"})
public class SummaryMarketDataResponse {

    @XmlElement(required = true)
    protected String eventSetId;
    @XmlElement(required = true)
    protected String sec;
    @XmlElement(required = true)
    protected String startDate;
    @XmlElement(required = true)
    protected String endDate;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlElement(required = true)
    protected String numberOfLines;


    public String getEventSetId() {
        return eventSetId;
    }
    public void setEventSetId(String value) {
        this.eventSetId = value;
    }

    public String getSec() {
    	return sec;
    }
    public void setSec(String value) {
    	this.sec = value;
    }
    
    public String getStartDate() {
    	return startDate;
    }
    public void setStartDate(String value) {
    	this.startDate = value;
    }
    
    public String getEndDate() {
    	return endDate;
    }
    public void setEndDate(String value) {
    	this.endDate = value;
    }
    
    public String getCurrencyCode() {
    	return currencyCode;
    }
    public void setCurrencyCode(String value) {
    	this.currencyCode = value;
    }
    
    public String getNumberOfLines() {
    	return numberOfLines;
    }
    public void setNumberOfLines(String value) {
    	this.numberOfLines = value;
    }
    
}