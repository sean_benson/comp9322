package au.edu.unsw.soacourse.service3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;









import au.edu.unsw.soacourse.service2.*;

@WebService(endpointInterface = "au.edu.unsw.soacourse.service3.MarketDataUtilService")
public class MarketDataUtilServiceImpl implements MarketDataUtilService {

	@Autowired
	private SOAPCurrencyConvertService service2 = new SOAPCurrencyConvertServiceImpl();
	
	public ConvertMarketDataResponseMsg ConvertMarketData(ConvertMarketDataRequestMsg request) throws ExchangeRateFaultMsg {
		
		ConvertMarketDataResponseMsg response = new ConvertMarketDataResponseMsg();
		
		ExchangeRateRequestMsg convertReq = new ExchangeRateRequestMsg();
		String baseCurrency = "AUD";
		convertReq.setTargetCurrencyCode(request.getTargetCurrency());
		String newEventSetId = request.getEventSetId();
		
		ServiceFaultType type = new ServiceFaultType();
		
		// Check if file is in different currency
		if (Pattern.matches(".*[A-Z]{3}$", request.getEventSetId())) {
			
			baseCurrency = request.getEventSetId().substring(Math.max(request.getEventSetId().length() - 3, 0));
//call the download file service
		
		//call the rate service
		//parse csv
		//convert columns
		//create new eventid
		//write file add currency to file name
		//write request
			newEventSetId = newEventSetId.substring(0, newEventSetId.length() - 3);
			
			if (baseCurrency.equals(request.getTargetCurrency())) {
				type.setErrtext("The file already contains converted prices");
				type.setErrcode("ERR_INV");
				throw new ExchangeRateFaultMsg("main_fault", type);
			}
			
		} else {
			if (request.getTargetCurrency().equals("AUD")) {
				type.setErrtext("The file already contains converted prices");
				type.setErrcode("ERR_INV");
				throw new ExchangeRateFaultMsg("main_fault", type);
			}
		}
		// Change filename
		if (!request.getTargetCurrency().equals("AUD")) newEventSetId = newEventSetId.concat(request.getTargetCurrency());
		
		try {
			convertReq.setBaseCurrencyCode(baseCurrency);
			double rate = Double.parseDouble((service2.yahooExchangeRate(convertReq).getRate()));
			
			File csv = new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+request.getEventSetId()+".csv");  
			Scanner s = new Scanner(csv);
			
		    FileWriter fstream = new FileWriter(new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+newEventSetId+".csv"), false); //false tells not to append data.
		    BufferedWriter out = new BufferedWriter(fstream);
		    
	        while (s.hasNextLine()) {
	        	String temp = s.nextLine();
	        	String data[] = temp.split(",",-1);
	        	
	        	DecimalFormat df = new DecimalFormat("###.##");	        	
	        	// Convert currency: baseCurrency * rate
	        	if (!data[5].equals("")) data[5]= df.format(Double.parseDouble(data[5])*rate).toString();
	        	if (!data[7].equals("")) data[7]=df.format(Double.parseDouble(data[7])*rate).toString();
	        	if (!data[9].equals("")) data[9]=df.format(Double.parseDouble(data[9])*rate).toString();
	        	
	        	int i = 0;
	        	while (i < data.length-1) {
		        	out.write(data[i]+",");
		        	i++;
	        	}
	        	out.write("\n");
			}
			s.close();
			out.close();
			
		} catch (ExchangeRateFaultMsg e) {
			type.setErrtext("ExchangeRateFaultMsg");
			type.setErrcode("ERR_INV");
			throw new ExchangeRateFaultMsg("Invalid Input", type);
		} catch (IOException g) {
			type.setErrtext("File not found, invalid eventSetId");
			type.setErrcode("ERR_INV");
			throw new ExchangeRateFaultMsg("Invalid Input", type);
		}
		response.setEventSetId(newEventSetId);
		return response;
	}

	@Override
	public SummaryMarketDataResponse SummaryMarketData(SummaryMarketDataRequest request) throws ExchangeRateFaultMsg {
		
		SummaryMarketDataResponse res = new SummaryMarketDataResponse();
		ServiceFaultType type = new ServiceFaultType();
		String eventSetId = request.getEventSetId();
		res.setEventSetId(eventSetId);
		
		String startDate = eventSetId.substring(0, 2)+"-"+eventSetId.substring(2, 4)+"-";
		if (Integer.parseInt(eventSetId.substring(4, 5)) >= 5) {
			startDate = startDate+"19"+eventSetId.substring(4, 6);
		} else {
			startDate = startDate+"20"+eventSetId.substring(4, 6);
		}
		res.setStartDate(startDate);
				
		String endDate = eventSetId.substring(6, 8)+"-"+eventSetId.substring(8, 10)+"-";
		if (Integer.parseInt(eventSetId.substring(10, 11)) >= 5) {
			endDate = endDate+"19"+eventSetId.substring(10, 12);
		} else {
			endDate = endDate+"20"+eventSetId.substring(10, 12);
		}
		res.setEndDate(endDate);

		String currency = "AUD";
		if (Pattern.matches(".*[A-Z]{3}$", request.getEventSetId())) {
			currency = request.getEventSetId().substring(Math.max(request.getEventSetId().length() - 3, 0));
		}

		try {
			File csv = new File(System.getProperty("catalina.home")+"/webapps/ROOT/"+request.getEventSetId()+".csv");
			Scanner s = new Scanner(csv);
			
			String sec = "";
			int numLines = 0;
			while (s.hasNextLine()) {
				numLines++;
				sec = s.nextLine().split(",")[0];
			}
			res.setSec(sec);
			res.setNumberOfLines(String.valueOf(numLines));

			s.close();
		} catch (FileNotFoundException e) {
			type.setErrtext("File not found, invalid eventSetId");
			type.setErrcode("ERR_INV");
			throw new ExchangeRateFaultMsg("Invalid Input", type);
		}
		
		
		res.setCurrencyCode(currency);		
		return res;
	}
}
