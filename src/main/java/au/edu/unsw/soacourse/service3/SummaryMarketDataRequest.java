package au.edu.unsw.soacourse.service3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "summaryMarketDataRequest", propOrder = {"eventSetId"})
public class SummaryMarketDataRequest {

    @XmlElement(required = true)
    protected String eventSetId;
    
    public String getEventSetId() {
        return this.eventSetId;
    }

    public void setEventSetId(String value) {
        this.eventSetId = value;
    }
    
}    