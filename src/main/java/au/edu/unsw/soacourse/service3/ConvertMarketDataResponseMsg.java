package au.edu.unsw.soacourse.service3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConvertMarketDataResponseMsg", propOrder = {"eventSetId"})
public class ConvertMarketDataResponseMsg {

    @XmlElement(required = true)
    protected String eventSetId;
    
    public String getEventSetId() {
        return eventSetId;
    }

    public void setEventSetId(String value) {
        this.eventSetId = value;
    }
}
