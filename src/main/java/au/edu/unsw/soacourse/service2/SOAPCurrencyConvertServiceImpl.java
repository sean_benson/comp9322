package au.edu.unsw.soacourse.service2;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.jws.WebService;

@WebService(endpointInterface = "au.edu.unsw.soacourse.service2.SOAPCurrencyConvertService")
public class SOAPCurrencyConvertServiceImpl implements SOAPCurrencyConvertService {

	@Override
	public ExchangeRateResponseMsg yahooExchangeRate(ExchangeRateRequestMsg request) throws ExchangeRateFaultMsg {

		ExchangeRateResponseMsg response = new ExchangeRateResponseMsg();
		ServiceFaultType fault = new ServiceFaultType();
		
		if (!Pattern.matches("^[A-Z]{3}",request.getBaseCurrencyCode()) ||
				!Pattern.matches("^[A-Z]{3}", request.getTargetCurrencyCode())) {
    		fault.setErrtext("Currency code should be exactly 3 characters long");
    		fault.setErrcode("ERR_INV");
    		throw new ExchangeRateFaultMsg("Invalid Input", fault);
		}
		if (request.getBaseCurrencyCode().equals(request.getTargetCurrencyCode())) {
			fault.setErrtext("Base currency code should be different from Target currency code");
			fault.setErrcode("ERR_INV");
			throw new ExchangeRateFaultMsg("Invalid Input", fault);
		}

		try {
			URL link = new URL("http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s="
					+request.getBaseCurrencyCode()+request.getTargetCurrencyCode()+"=X");
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int n = 0;
			while (-1 != (n=in.read(buf))) {
			   out.write(buf, 0, n);
			}
			out.close();
			in.close();
			byte[] message = out.toByteArray();
			String line = new String(message);
			
			Scanner scanner = new Scanner(line);
			scanner.useDelimiter(",");
			scanner.next();
			response.setRate(scanner.next());
			response.setAsAt(scanner.next().replaceAll("/", "-").replaceAll("\\\"", ""));
			scanner.close();
		} catch (IOException e) {}
			
		return response;
	}
}
