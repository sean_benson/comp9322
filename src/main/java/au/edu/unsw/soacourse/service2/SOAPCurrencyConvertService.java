package au.edu.unsw.soacourse.service2;

import javax.jws.WebService;

@WebService
public interface SOAPCurrencyConvertService {

	public ExchangeRateResponseMsg yahooExchangeRate(ExchangeRateRequestMsg request) throws ExchangeRateFaultMsg ;
	
}
