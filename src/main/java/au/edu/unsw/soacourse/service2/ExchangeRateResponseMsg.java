package au.edu.unsw.soacourse.service2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRateResponseMsg", propOrder = {"rate", "asAt"})
public class ExchangeRateResponseMsg {

    @XmlElement(required = true)
    protected String rate;
    
    @XmlElement(required = true)
    protected String asAt;

    public String getRate() {
        return rate;
    }

    public void setRate(String value) {
        this.rate = value;
    }

    public String getAsAt() {
    	return asAt;
    }
    
    public void setAsAt(String value) {
    	this.asAt = value;
    }
}
