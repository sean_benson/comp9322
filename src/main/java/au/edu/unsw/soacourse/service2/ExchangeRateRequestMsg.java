package au.edu.unsw.soacourse.service2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRateRequestMsg", propOrder = {"baseCurrencyCode", "targetCurrencyCode"})
public class ExchangeRateRequestMsg {

    @XmlElement(required = true)
    protected String baseCurrencyCode;
    
    @XmlElement(required = true)
    protected String targetCurrencyCode;

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String value) {
        this.baseCurrencyCode = value;
    }

    public String getTargetCurrencyCode() {
    	return targetCurrencyCode;
    }
    
    public void setTargetCurrencyCode(String value) {
    	this.targetCurrencyCode = value;
    }
}


